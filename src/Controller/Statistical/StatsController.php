<?php

namespace App\Controller\Statistical;

use App\Document\Student;
use App\Repository\StudentRepository;
use Doctrine\ODM\MongoDB\DocumentManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class StatsController
 * @package App\Controller\Statistical
 * @author Jean Marius <jean.marius@dyosis.com>
 * @Route("/statistics")
 */
class StatsController extends AbstractController
{
	/**
	 * Global action.
	 *
	 * @Route("/global", name="app_statistics_global")
	 * @return Response
	 */
	public function globalAction(DocumentManager $manager)
	{
		/** @var StudentRepository $repository */
		$repository = $manager->getRepository(Student::class);

		$bestStudents = $repository->findBy([], ['averageMark' => 'DESC'], 3);
		$frequencyCities = $repository->getCitiesFrequency();
		$mostImprovementSupinfoWord = $repository->findBestImprovementSupinfoWord();
		$mostSupinfoConsciousness = $repository->findBestSupinfoConsciousness();
		$averageHiringTime = $repository->findAverageHiringTime();
		$bestEnterpriseHire = $repository->findBestEnterpriseHire();
		$bestStudentByCities = [];
		foreach ($frequencyCities as $city => $number) {
			$bestStudentByCities[$city] = $repository->findBestByCity($city);
		}

		return $this->render('Page/Statistics/Stats/index.html.twig', [
			'best_students' => $bestStudents,
			'frequency_cities' => $frequencyCities,
			'most_improvement_supinfo_word' => $mostImprovementSupinfoWord,
			'most_supinfo_consciousness' => $mostSupinfoConsciousness,
			'average_hiring_time' => $averageHiringTime,
			'best_enterprise_hire' => $bestEnterpriseHire,
			'best_students_by_cities' => $bestStudentByCities
		]);
	}
}
