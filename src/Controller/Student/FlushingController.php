<?php

namespace App\Controller\Student;

use App\Document\Student;
use App\Service\Util\Console\Console;
use App\Service\Util\Console\Model\Message;
use Doctrine\ODM\MongoDB\DocumentManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class FlushingController
 * @package App\Controller\Student
 * @author Jean Marius <jean.marius@dyosis.com>
 * @Route("/student")
 */
class FlushingController extends AbstractController
{
	/**
	 * Flush student.
	 *
	 * @param DocumentManager $manager
	 * @param Console $console
	 * @return RedirectResponse
	 * @throws \Doctrine\ODM\MongoDB\MongoDBException
	 * @Route("/flush", name="app_student_flush")
	 */
	public function flushStudent(DocumentManager $manager, Console $console)
	{
		$students = $manager->getRepository(Student::class)->findAll();
		foreach ($students as $student) {
			$manager->remove($student);
		}
		$manager->flush();
		$console->add('La base de données a correctement été vidée', Message::TYPE_SUCCESS);

		return $this->redirectToRoute('app_home_index');
	}
}
