<?php

namespace App\Controller\Student;

use App\Service\Util\Console\Console;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Form\Model\Student\Importing\ImportStudentModel;
use App\Form\Type\Student\Importing\ImportStudentType;
use Symfony\Component\HttpFoundation\Request;
use App\Service\Business\StudentBusiness;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ImportingController
 * @package App\Controller\Student
 * @Route("/student")
 */
class ImportingController extends AbstractController
{
	/**
	 * @Route("/import", name="app_student_import")
	 *
	 * @param Request $request
	 * @param StudentBusiness $studentBusiness
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function importStudents(Request $request, StudentBusiness $studentBusiness)
	{
		$importStudentModel = new ImportStudentModel();
		$form = $this->createForm(ImportStudentType::class, $importStudentModel);
		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$studentBusiness->importStudentsFromCSV($importStudentModel->getFile());
		}

		return $this->render('Page/Student/Importing/import_students.html.twig', [
			'form' => $form->createView(),
		]);
	}
}
