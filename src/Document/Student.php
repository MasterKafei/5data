<?php

namespace App\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use App\Repository\StudentRepository;

/**
 * Class Student
 * @package App\Entity
 * @author Jean Marius <jean.marius@dyosis.com>
 * @MongoDB\Document(repositoryClass=StudentRepository::class)
 */
class Student
{
	/**
	 * @var int
	 * @MongoDB\Id
	 */
	private $id;

	/**
	 * @var int
	 * @MongoDB\Field(type="integer")
	 */
	private $idBooster;

	/**
	 * @var string
	 * @MongoDB\Field(type="string")
	 */
	private $city;

	/**
	 * @var string
	 * @MongoDB\Field(type="string")
	 */
	private $previousInstitution;

	/**
	 * @var string
	 * @MongoDB\Field(type="int")
	 */
	private $inscriptionYear;

	/**
	 * @var string|null
	 * @MongoDB\Field(type="string", nullable=true)
	 */
	private $enterprise;

	/**
	 * @var string
	 * @MongoDB\Field(type="int")
	 */
	private $graduatedYear;

	/**
	 * @var string
	 * @MongoDB\Field(type="string")
	 */
	private $enterpriseAfterGraduation;

	/**
	 * @var float
	 * @MongoDB\Field(type="float")
	 */
	private $averageMark;

	/**
	 * @var string
	 * @MongoDB\Field(type="string")
	 */
	private $supinfoImprovementWord;

	/**
	 * @var string
	 * @MongoDB\Field(type="string")
	 */
	private $hireYear;

	/**
	 * @var string
	 * @MongoDB\Field(type="string")
	 */
	private $supinfoConsciousness;

	/**
	 * Get id.
	 *
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @return int
	 */
	public function getIdBooster(): int
	{
		return $this->idBooster;
	}

	/**
	 * @param int $idBooster
	 *
	 * @return Student
	 */
	public function setIdBooster(int $idBooster)
	{
		$this->idBooster = $idBooster;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getCity(): string
	{
		return $this->city;
	}

	/**
	 * @param string $city
	 *
	 * @return Student
	 */
	public function setCity(string $city)
	{
		$this->city = $city;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getPreviousInstitution(): string
	{
		return $this->previousInstitution;
	}

	/**
	 * @param string $previousInstitution
	 *
	 * @return Student
	 */
	public function setPreviousInstitution(string $previousInstitution)
	{
		$this->previousInstitution = $previousInstitution;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getInscriptionYear(): string
	{
		return $this->inscriptionYear;
	}

	/**
	 * @param string $inscriptionYear
	 *
	 * @return Student
	 */
	public function setInscriptionYear(string $inscriptionYear)
	{
		$this->inscriptionYear = $inscriptionYear;
		return $this;
	}

	/**
	 * @return null|string
	 */
	public function getEnterprise(): ?string
	{
		return $this->enterprise;
	}

	/**
	 * @param null|string $enterprise
	 *
	 * @return Student
	 */
	public function setEnterprise(?string $enterprise)
	{
		$this->enterprise = $enterprise;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getGraduatedYear(): string
	{
		return $this->graduatedYear;
	}

	/**
	 * @param string $graduatedYear
	 *
	 * @return Student
	 */
	public function setGraduatedYear(string $graduatedYear)
	{
		$this->graduatedYear = $graduatedYear;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getEnterpriseAfterGraduation(): string
	{
		return $this->enterpriseAfterGraduation;
	}

	/**
	 * @param string $enterpriseAfterGraduation
	 *
	 * @return Student
	 */
	public function setEnterpriseAfterGraduation(string $enterpriseAfterGraduation)
	{
		$this->enterpriseAfterGraduation = $enterpriseAfterGraduation;
		return $this;
	}

	/**
	 * @return float
	 */
	public function getAverageMark(): float
	{
		return $this->averageMark;
	}

	/**
	 * @param float $averageMark
	 *
	 * @return Student
	 */
	public function setAverageMark(float $averageMark)
	{
		$this->averageMark = $averageMark;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getSupinfoImprovementWord(): string
	{
		return $this->supinfoImprovementWord;
	}

	/**
	 * @param string $supinfoImprovementWord
	 *
	 * @return Student
	 */
	public function setSupinfoImprovementWord(string $supinfoImprovementWord)
	{
		$this->supinfoImprovementWord = $supinfoImprovementWord;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getHireYear(): string
	{
		return $this->hireYear;
	}

	/**
	 * @param string $hireYear
	 *
	 * @return Student
	 */
	public function setHireYear(string $hireYear)
	{
		$this->hireYear = $hireYear;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getSupinfoConsciousness(): string
	{
		return $this->supinfoConsciousness;
	}

	/**
	 * @param string $supinfoConsciousness
	 *
	 * @return Student
	 */
	public function setSupinfoConsciousness(string $supinfoConsciousness)
	{
		$this->supinfoConsciousness = $supinfoConsciousness;
		return $this;
	}
}