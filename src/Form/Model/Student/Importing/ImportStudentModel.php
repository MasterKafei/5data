<?php

namespace App\Form\Model\Student\Importing;

use Symfony\Component\HttpFoundation\File\File;

class ImportStudentModel
{
	/**
	 * @var File
	 */
	private $file;

	/**
	 * Get file.
	 *
	 * @return File
	 */
	public function getFile()
	{
		return $this->file;
	}

	/**
	 * Set file.
	 *
	 * @param $file
	 * @return $this
	 */
	public function setFile($file)
	{
		$this->file = $file;

		return $this;
	}
}
