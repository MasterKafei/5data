<?php

namespace App\Repository;

use App\Document\Student;
use Doctrine\ODM\MongoDB\Repository\DocumentRepository;

/**
 * Class StudentRepository
 * @package App\Repository
 * @author Jean Marius <jean.marius@dyosis.com>
 */
class StudentRepository extends DocumentRepository
{
	/**
	 * Find all city.
	 *
	 * @return string[]
	 */
	public function findAllCity()
	{
		$this->createQueryBuilder()
			->distinct('city')
			->getQuery()
			->execute();
	}

	/**
	 * Find all previous institution.
	 *
	 * @return string[]
	 */
	public function findAllPreviousInstitution()
	{
		$this->createQueryBuilder()
			->distinct('previousInstitution')
			->getQuery()
			->execute();
	}


	/**
	 * Find best by city.
	 *
	 * @param string $city
	 * @param int $maxResult
	 * @return Student[]
	 */
	public function findBestByCity($city, $maxResult = 3)
	{
		return $this
			->createQueryBuilder()
			->field('city')
			->equals($city)
			->sort('averageMark', 'DESC')
			->limit($maxResult)
			->getQuery()
			->execute();
	}

	/**
	 * Find best by previous institution.
	 *
	 * @param $previousInstitution
	 * @param $maxResult
	 * @return
	 */
	public function findBestByPreviousInstitution($previousInstitution, $maxResult = 3)
	{
		return $this->createQueryBuilder()
			->field('previousInstitution')->equals($previousInstitution)
			->sort('averageMark', 'DESC')
			->limit($maxResult)
			->getQuery()
			->execute();
	}

	/**
	 * Find best improvement supinfo word.
	 *
	 * @return string
	 */
	public function findBestImprovementSupinfoWord()
	{
		/** @var Student[] $content */
		$content = $this->findAll();
		$count = [];
		$max = 0;
		$current = null;
		foreach ($content as $value) {
			$number = ($count[$value->getSupinfoImprovementWord()] ?? 0) + 1;
			$count[$value->getSupinfoImprovementWord()] = $number;
			if ($number > $max) {
				$max = $number;
				$current = $value->getSupinfoImprovementWord();
			}
		}

		return $current;
	}

	/**
	 * Find best supinfo consciousness.
	 *
	 * @return string
	 */
	public function findBestSupinfoConsciousness()
	{
		/** @var Student[] $content */
		$content = $this->findAll();
		$count = [];
		$max = 0;
		$current = null;
		foreach ($content as $value) {
			$number = ($count[$value->getSupinfoConsciousness()] ?? 0) + 1;
			$count[$value->getSupinfoConsciousness()] = $number;
			if ($number > $max) {
				$max = $number;
				$current = $value->getSupinfoConsciousness();
			}
		}

		return $current;
	}

	/**
	 * Find average hiring time.
	 *
	 * @return string
	 */
	public function findAverageHiringTime()
	{
		/** @var Student[] $content */
		$content = $this->findAll();
		$count = 0;
		$number = 0;
		$current = null;
		foreach ($content as $value) {
			$number += intval($value->getHireYear()) - intval($value->getGraduatedYear());
			++$count;
		}

		return $number / $count;
	}

	public function getCitiesFrequency()
	{
		/** @var Student[] $content */
		$content = $this->findAll();
		$count = [];
		foreach ($content as $value) {
			$number = ($count[$value->getCity()] ?? 0) + 1;
			$count[$value->getCity()] = $number;
		}

		return $count;
	}

	/**
	 * Find best enterprise hire.
	 *
	 * @return string
	 */
	public function findBestEnterpriseHire()
	{
		/** @var Student[] $content */
		$content = $this->findAll();
		$count = [];
		$max = 0;
		$current = null;
		foreach ($content as $value) {
			$number = ($count[$value->getEnterprise()] ?? 0) + 1;
			$count[$value->getEnterprise()] = $number;
			if ($number > $max) {
				$max = $number;
				$current = $value->getEnterprise();
			}
		}

		return $current;
	}
}
