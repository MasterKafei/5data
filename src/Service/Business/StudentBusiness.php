<?php

namespace App\Service\Business;


use App\Document\Student;
use App\Service\Util\Console\Console;
use App\Service\Util\Console\Model\Message;
use App\Service\Util\CSVParser;
use Doctrine\ODM\MongoDB\DocumentManager;
use Symfony\Component\HttpFoundation\File\File;

/**
 * Class StudentBusiness
 * @package App\Service\Business
 * @author Jean Marius <jean.marius@dyosis.com>
 * @author Bertrand Massieux <bertrand.massieux@dyosis.com>
 */
class StudentBusiness
{
	const ACCEPTED_CSV_MIME_TYPE = [
		'text/csv',
		'text/plain',
	];

	/**
	 * @var CSVParser
	 */
	private $parser;

	/**
	 * @var DocumentManager
	 */
	private $manager;

	/**
	 * @var Console
	 */
	private $console;

	/**
	 * Constructor.
	 *
	 * @param CSVParser $parser
	 * @param DocumentManager $manager
	 * @param Console $console
	 */
	public function __construct(CSVParser $parser, DocumentManager $manager, Console $console)
	{
		$this->parser = $parser;
		$this->manager = $manager;
		$this->console = $console;
	}

	public function importStudentsFromCSV(File $file)
	{
		$this->isCSVAccepted($file);
		$data = $this->parser->parseFile($file);

		$number = 0;
		foreach ($data as $datum) {
			++$number;
			$idBooster = $datum['idBooster'];
			$city = $datum['cityOrigin'];
			$previousInstitution = $datum['previousInstitution'];
			$inscriptionYear = $datum['inscriptionYear'];
			$enterprise = $datum['enterprise'];
			$graduatedYear = $datum['graduatedYear'];
			$enterpriseAfterGraduation = $datum['enterpriseAfterGraduation'];
			$averageMark = $datum['averageMark'];
			$supinfoImprovementWord = $datum['supinfoImprovementWord'];
			$hireYear = $datum['hireYear'];
			$supinfoConsciousness = $datum['supinfoConsciousness'];

			$student = (new Student())
				->setIdBooster($idBooster)
				->setCity($city)
				->setPreviousInstitution($previousInstitution)
				->setInscriptionYear($inscriptionYear)
				->setEnterprise($enterprise)
				->setGraduatedYear($graduatedYear)
				->setEnterprise($enterprise)
				->setEnterprise($enterpriseAfterGraduation)
				->setAverageMark($averageMark)
				->setSupinfoImprovementWord($supinfoImprovementWord)
				->setHireYear($hireYear)
				->setSupinfoConsciousness($supinfoConsciousness)
			;
			$this->manager->persist($student);
		}
		$this->manager->flush();
		if($number > 1)  {
			$this->console->add("L'import s'est bien passé, $number nouvelles lignes ont été ajoutées", Message::TYPE_SUCCESS);
		} else {
			$this->console->add("L'import s'est bien passé, $number nouvelle ligne ont été ajoutée",Message::TYPE_SUCCESS);
		}

	}

	protected function isCSVAccepted(File $file)
	{
		if (!in_array($file->getMimeType(), self::ACCEPTED_CSV_MIME_TYPE)) {
			throw new \RuntimeException('Unsupported file type form student import, please check your validation file');
		}
	}
}
