<?php

namespace App\Service\Util;

use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Serializer\Encoder\DecoderInterface;

class CSVParser
{
	/**
	 * @var DecoderInterface
	 */
	private $decoder;

	/**
	 * Constructor.
	 *
	 * @param DecoderInterface $decoder
	 */
	public function __construct(DecoderInterface $decoder)
	{
		$this->decoder = $decoder;
	}

	public function parseFile(File $file)
	{
		$data = file_get_contents($file->getPathname());

		return $this->decoder->decode($data, 'csv');
	}
}
